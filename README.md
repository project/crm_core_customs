# CRM Core Customs
Customizations for CRM Core module

## Relationship Ui Autocomplete
By default, appends field's "Father name" value, or any other field's value that you can configure in module's administration page, next to "Contact name" that appears in Relationship Ui autocomplete forms, for better contact identification.

**Important!** Fow nor you need to create that field manually in your preferred CRM Core contact type (i.e. *Individual*).
